import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
  apiKey: "AIzaSyBEqkNwmqJfWXLD1ZZmu2VhxyDEsLaGchw",
  authDomain: "crwn-db-ecb6a.firebaseapp.com",
  databaseURL: "https://crwn-db-ecb6a.firebaseio.com",
  projectId: "crwn-db-ecb6a",
  storageBucket: "crwn-db-ecb6a.appspot.com",
  messagingSenderId: "941869855174",
  appId: "1:941869855174:web:410b566f34c7a0359a82c7"
};

firebase.initializeApp(config);

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`);

  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();
    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      });
    } catch (error) {
      console.log('error creating user', error.message);
    }
  }

  return userRef;
};

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
